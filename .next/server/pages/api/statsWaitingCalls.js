/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/api/statsWaitingCalls";
exports.ids = ["pages/api/statsWaitingCalls"];
exports.modules = {

/***/ "./pages/api/statsWaitingCalls.js":
/*!****************************************!*\
  !*** ./pages/api/statsWaitingCalls.js ***!
  \****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst axios = __webpack_require__(/*! axios */ \"axios\");\n\nconst qs = __webpack_require__(/*! qs */ \"qs\");\n\nconst config = {\n  headers: {\n    'Content-Type': 'application/x-www-form-urlencoded'\n  }\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (async (req, res) => {\n  try {\n    const {\n      tenant,\n      session\n    } = req.body;\n    const data = {\n      session: session\n    }; //To get data from statsWaitingCalls API\n\n    const response = await axios.post(`${tenant}?function=statsWaitingCalls`, qs.stringify(data), config);\n\n    if (response.status == 404) {\n      return res.status(200).json({\n        code: 401,\n        message: \"UNAUTHORIZED\"\n      });\n    } else if (response.status != 200) {\n      return res.status(200).json({\n        code: 401,\n        message: \"UNAUTHORIZED\"\n      });\n    } else if (response.data.success) {\n      return res.status(200).json({\n        code: 200,\n        success: response.data.success,\n        waiting_calls: response.data.stats.waiting_calls.length > 0 ? response.data.stats.waiting_calls.waiting_count : 0\n      });\n    }\n  } catch (error) {\n    return res.status(500).json({\n      message: 'An error occurred during login'\n    });\n  }\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zYW1wbGUtd2FsbGJvYXJkLy4vcGFnZXMvYXBpL3N0YXRzV2FpdGluZ0NhbGxzLmpzPzYwOTQiXSwibmFtZXMiOlsiYXhpb3MiLCJyZXF1aXJlIiwicXMiLCJjb25maWciLCJoZWFkZXJzIiwicmVxIiwicmVzIiwidGVuYW50Iiwic2Vzc2lvbiIsImJvZHkiLCJkYXRhIiwicmVzcG9uc2UiLCJwb3N0Iiwic3RyaW5naWZ5Iiwic3RhdHVzIiwianNvbiIsImNvZGUiLCJtZXNzYWdlIiwic3VjY2VzcyIsIndhaXRpbmdfY2FsbHMiLCJzdGF0cyIsImxlbmd0aCIsIndhaXRpbmdfY291bnQiLCJlcnJvciJdLCJtYXBwaW5ncyI6IjtBQUFBLE1BQU1BLEtBQUssR0FBR0MsbUJBQU8sQ0FBQyxvQkFBRCxDQUFyQjs7QUFDQSxNQUFNQyxFQUFFLEdBQUdELG1CQUFPLENBQUMsY0FBRCxDQUFsQjs7QUFFQSxNQUFNRSxNQUFNLEdBQUc7QUFDWEMsU0FBTyxFQUFFO0FBQ0wsb0JBQWdCO0FBRFg7QUFERSxDQUFmO0FBS0EsK0RBQWUsT0FBUUMsR0FBUixFQUFhQyxHQUFiLEtBQXFCO0FBQ2hDLE1BQUk7QUFDQSxVQUFNO0FBQUVDLFlBQUY7QUFBVUM7QUFBVixRQUFzQkgsR0FBRyxDQUFDSSxJQUFoQztBQUNBLFVBQU1DLElBQUksR0FBRztBQUNURixhQUFPLEVBQUVBO0FBREEsS0FBYixDQUZBLENBS0E7O0FBQ0EsVUFBTUcsUUFBUSxHQUFHLE1BQU1YLEtBQUssQ0FBQ1ksSUFBTixDQUFZLEdBQUVMLE1BQU8sNkJBQXJCLEVBQW1ETCxFQUFFLENBQUNXLFNBQUgsQ0FBYUgsSUFBYixDQUFuRCxFQUF1RVAsTUFBdkUsQ0FBdkI7O0FBRUEsUUFBSVEsUUFBUSxDQUFDRyxNQUFULElBQW1CLEdBQXZCLEVBQTJCO0FBQ3ZCLGFBQU9SLEdBQUcsQ0FBQ1EsTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVDLFlBQUksRUFBRSxHQUFSO0FBQWFDLGVBQU8sRUFBRTtBQUF0QixPQUFyQixDQUFQO0FBQ0gsS0FGRCxNQUVNLElBQUlOLFFBQVEsQ0FBQ0csTUFBVCxJQUFtQixHQUF2QixFQUEyQjtBQUM3QixhQUFPUixHQUFHLENBQUNRLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQjtBQUFFQyxZQUFJLEVBQUUsR0FBUjtBQUFhQyxlQUFPLEVBQUU7QUFBdEIsT0FBckIsQ0FBUDtBQUNILEtBRkssTUFHRCxJQUFJTixRQUFRLENBQUNELElBQVQsQ0FBY1EsT0FBbEIsRUFBMkI7QUFDNUIsYUFBT1osR0FBRyxDQUFDUSxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFDeEJDLFlBQUksRUFBRSxHQURrQjtBQUV4QkUsZUFBTyxFQUFFUCxRQUFRLENBQUNELElBQVQsQ0FBY1EsT0FGQztBQUd4QkMscUJBQWEsRUFBRVIsUUFBUSxDQUFDRCxJQUFULENBQWNVLEtBQWQsQ0FBb0JELGFBQXBCLENBQWtDRSxNQUFsQyxHQUEyQyxDQUEzQyxHQUErQ1YsUUFBUSxDQUFDRCxJQUFULENBQWNVLEtBQWQsQ0FBb0JELGFBQXBCLENBQWtDRyxhQUFqRixHQUFpRztBQUh4RixPQUFyQixDQUFQO0FBS0g7QUFDSixHQXBCRCxDQW9CRSxPQUFPQyxLQUFQLEVBQWM7QUFDWixXQUFPakIsR0FBRyxDQUFDUSxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFBRUUsYUFBTyxFQUFFO0FBQVgsS0FBckIsQ0FBUDtBQUNIO0FBQ0osQ0F4QkQiLCJmaWxlIjoiLi9wYWdlcy9hcGkvc3RhdHNXYWl0aW5nQ2FsbHMuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBheGlvcyA9IHJlcXVpcmUoJ2F4aW9zJyk7XHJcbmNvbnN0IHFzID0gcmVxdWlyZSgncXMnKTtcclxuXHJcbmNvbnN0IGNvbmZpZyA9IHtcclxuICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCdcclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBhc3luYyAgKHJlcSwgcmVzKSA9PiB7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIGNvbnN0IHsgdGVuYW50LCBzZXNzaW9uIH0gPSByZXEuYm9keTtcclxuICAgICAgICBjb25zdCBkYXRhID0ge1xyXG4gICAgICAgICAgICBzZXNzaW9uOiBzZXNzaW9uXHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vVG8gZ2V0IGRhdGEgZnJvbSBzdGF0c1dhaXRpbmdDYWxscyBBUElcclxuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGF4aW9zLnBvc3QoYCR7dGVuYW50fT9mdW5jdGlvbj1zdGF0c1dhaXRpbmdDYWxsc2AsIHFzLnN0cmluZ2lmeShkYXRhKSwgY29uZmlnKTtcclxuICAgICAgICBcclxuICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09IDQwNCl7XHJcbiAgICAgICAgICAgIHJldHVybiByZXMuc3RhdHVzKDIwMCkuanNvbih7IGNvZGU6IDQwMSwgbWVzc2FnZTogXCJVTkFVVEhPUklaRURcIn0pXHJcbiAgICAgICAgfWVsc2UgaWYgKHJlc3BvbnNlLnN0YXR1cyAhPSAyMDApe1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzLnN0YXR1cygyMDApLmpzb24oeyBjb2RlOiA0MDEsIG1lc3NhZ2U6IFwiVU5BVVRIT1JJWkVEXCJ9KVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmIChyZXNwb25zZS5kYXRhLnN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHJlcy5zdGF0dXMoMjAwKS5qc29uKHtcclxuICAgICAgICAgICAgICAgIGNvZGU6IDIwMCxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IHJlc3BvbnNlLmRhdGEuc3VjY2VzcyxcclxuICAgICAgICAgICAgICAgIHdhaXRpbmdfY2FsbHM6IHJlc3BvbnNlLmRhdGEuc3RhdHMud2FpdGluZ19jYWxscy5sZW5ndGggPiAwID8gcmVzcG9uc2UuZGF0YS5zdGF0cy53YWl0aW5nX2NhbGxzLndhaXRpbmdfY291bnQgOiAwXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcclxuICAgICAgICByZXR1cm4gcmVzLnN0YXR1cyg1MDApLmpzb24oeyBtZXNzYWdlOiAnQW4gZXJyb3Igb2NjdXJyZWQgZHVyaW5nIGxvZ2luJyB9KVxyXG4gICAgfVxyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/api/statsWaitingCalls.js\n");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("axios");;

/***/ }),

/***/ "qs":
/*!*********************!*\
  !*** external "qs" ***!
  \*********************/
/***/ (function(module) {

"use strict";
module.exports = require("qs");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/api/statsWaitingCalls.js"));
module.exports = __webpack_exports__;

})();