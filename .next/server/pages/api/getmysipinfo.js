/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/api/getmysipinfo";
exports.ids = ["pages/api/getmysipinfo"];
exports.modules = {

/***/ "./pages/api/getmysipinfo.js":
/*!***********************************!*\
  !*** ./pages/api/getmysipinfo.js ***!
  \***********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst axios = __webpack_require__(/*! axios */ \"axios\");\n\nconst qs = __webpack_require__(/*! qs */ \"qs\");\n\nconst config = {\n  headers: {\n    'Content-Type': 'application/x-www-form-urlencoded'\n  }\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (async (req, res) => {\n  try {\n    const {\n      tenant,\n      session\n    } = req.body;\n    const data = {\n      session: session\n    };\n    const response = await axios.post(`${tenant}?function=getmysipinfo`, qs.stringify(data), config);\n\n    if (response.status == 404) {\n      return res.status(200).json({\n        code: 401,\n        message: \"UNAUTHORIZED\"\n      });\n    } else if (response.status != 200) {\n      return res.status(200).json({\n        code: 401,\n        message: \"UNAUTHORIZED\"\n      });\n    } else {\n      return res.status(200).json({\n        code: 200,\n        success: response.data.success,\n        name: response.data.name\n      });\n    }\n  } catch (error) {\n    return res.status(500).json({\n      message: 'An error occurred during login'\n    });\n  }\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zYW1wbGUtd2FsbGJvYXJkLy4vcGFnZXMvYXBpL2dldG15c2lwaW5mby5qcz8zOTVhIl0sIm5hbWVzIjpbImF4aW9zIiwicmVxdWlyZSIsInFzIiwiY29uZmlnIiwiaGVhZGVycyIsInJlcSIsInJlcyIsInRlbmFudCIsInNlc3Npb24iLCJib2R5IiwiZGF0YSIsInJlc3BvbnNlIiwicG9zdCIsInN0cmluZ2lmeSIsInN0YXR1cyIsImpzb24iLCJjb2RlIiwibWVzc2FnZSIsInN1Y2Nlc3MiLCJuYW1lIiwiZXJyb3IiXSwibWFwcGluZ3MiOiI7QUFBQSxNQUFNQSxLQUFLLEdBQUdDLG1CQUFPLENBQUMsb0JBQUQsQ0FBckI7O0FBQ0EsTUFBTUMsRUFBRSxHQUFHRCxtQkFBTyxDQUFDLGNBQUQsQ0FBbEI7O0FBRUEsTUFBTUUsTUFBTSxHQUFHO0FBQ1hDLFNBQU8sRUFBRTtBQUNMLG9CQUFnQjtBQURYO0FBREUsQ0FBZjtBQUtBLCtEQUFlLE9BQVFDLEdBQVIsRUFBYUMsR0FBYixLQUFxQjtBQUNoQyxNQUFJO0FBQ0EsVUFBTTtBQUFFQyxZQUFGO0FBQVVDO0FBQVYsUUFBc0JILEdBQUcsQ0FBQ0ksSUFBaEM7QUFDQSxVQUFNQyxJQUFJLEdBQUc7QUFDVEYsYUFBTyxFQUFFQTtBQURBLEtBQWI7QUFJQSxVQUFNRyxRQUFRLEdBQUcsTUFBTVgsS0FBSyxDQUFDWSxJQUFOLENBQVksR0FBRUwsTUFBTyx3QkFBckIsRUFBOENMLEVBQUUsQ0FBQ1csU0FBSCxDQUFhSCxJQUFiLENBQTlDLEVBQWtFUCxNQUFsRSxDQUF2Qjs7QUFFQSxRQUFJUSxRQUFRLENBQUNHLE1BQVQsSUFBbUIsR0FBdkIsRUFBMkI7QUFDdkIsYUFBT1IsR0FBRyxDQUFDUSxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFBRUMsWUFBSSxFQUFFLEdBQVI7QUFBYUMsZUFBTyxFQUFFO0FBQXRCLE9BQXJCLENBQVA7QUFDSCxLQUZELE1BRU0sSUFBSU4sUUFBUSxDQUFDRyxNQUFULElBQW1CLEdBQXZCLEVBQTJCO0FBQzdCLGFBQU9SLEdBQUcsQ0FBQ1EsTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVDLFlBQUksRUFBRSxHQUFSO0FBQWFDLGVBQU8sRUFBRTtBQUF0QixPQUFyQixDQUFQO0FBQ0gsS0FGSyxNQUVBO0FBQ0YsYUFBT1gsR0FBRyxDQUFDUSxNQUFKLENBQVcsR0FBWCxFQUFnQkMsSUFBaEIsQ0FBcUI7QUFBQ0MsWUFBSSxFQUFFLEdBQVA7QUFBWUUsZUFBTyxFQUFFUCxRQUFRLENBQUNELElBQVQsQ0FBY1EsT0FBbkM7QUFBNENDLFlBQUksRUFBRVIsUUFBUSxDQUFDRCxJQUFULENBQWNTO0FBQWhFLE9BQXJCLENBQVA7QUFDSDtBQUNKLEdBZkQsQ0FlRSxPQUFPQyxLQUFQLEVBQWM7QUFDWixXQUFPZCxHQUFHLENBQUNRLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxJQUFoQixDQUFxQjtBQUFFRSxhQUFPLEVBQUU7QUFBWCxLQUFyQixDQUFQO0FBQ0g7QUFDSixDQW5CRCIsImZpbGUiOiIuL3BhZ2VzL2FwaS9nZXRteXNpcGluZm8uanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBheGlvcyA9IHJlcXVpcmUoJ2F4aW9zJyk7XG5jb25zdCBxcyA9IHJlcXVpcmUoJ3FzJyk7XG5cbmNvbnN0IGNvbmZpZyA9IHtcbiAgICBoZWFkZXJzOiB7XG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJ1xuICAgIH1cbn1cbmV4cG9ydCBkZWZhdWx0IGFzeW5jICAocmVxLCByZXMpID0+IHtcbiAgICB0cnkge1xuICAgICAgICBjb25zdCB7IHRlbmFudCwgc2Vzc2lvbiB9ID0gcmVxLmJvZHk7XG4gICAgICAgIGNvbnN0IGRhdGEgPSB7XG4gICAgICAgICAgICBzZXNzaW9uOiBzZXNzaW9uXG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGF4aW9zLnBvc3QoYCR7dGVuYW50fT9mdW5jdGlvbj1nZXRteXNpcGluZm9gLCBxcy5zdHJpbmdpZnkoZGF0YSksIGNvbmZpZyk7XG4gICAgICBcbiAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PSA0MDQpe1xuICAgICAgICAgICAgcmV0dXJuIHJlcy5zdGF0dXMoMjAwKS5qc29uKHsgY29kZTogNDAxLCBtZXNzYWdlOiBcIlVOQVVUSE9SSVpFRFwifSlcbiAgICAgICAgfWVsc2UgaWYgKHJlc3BvbnNlLnN0YXR1cyAhPSAyMDApe1xuICAgICAgICAgICAgcmV0dXJuIHJlcy5zdGF0dXMoMjAwKS5qc29uKHsgY29kZTogNDAxLCBtZXNzYWdlOiBcIlVOQVVUSE9SSVpFRFwifSlcbiAgICAgICAgfWVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHJlcy5zdGF0dXMoMjAwKS5qc29uKHtjb2RlOiAyMDAsIHN1Y2Nlc3M6IHJlc3BvbnNlLmRhdGEuc3VjY2VzcywgbmFtZTogcmVzcG9uc2UuZGF0YS5uYW1lfSlcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIHJldHVybiByZXMuc3RhdHVzKDUwMCkuanNvbih7IG1lc3NhZ2U6ICdBbiBlcnJvciBvY2N1cnJlZCBkdXJpbmcgbG9naW4nIH0pXG4gICAgfVxufSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/api/getmysipinfo.js\n");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("axios");;

/***/ }),

/***/ "qs":
/*!*********************!*\
  !*** external "qs" ***!
  \*********************/
/***/ (function(module) {

"use strict";
module.exports = require("qs");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/api/getmysipinfo.js"));
module.exports = __webpack_exports__;

})();