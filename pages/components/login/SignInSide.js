import React, { useEffect, useState } from "react";
import { Spin, Alert } from 'antd'; 
import Head from 'next/head';
import Image from 'next/image';
import axios from "axios";

import styles from '../../../styles/SignInSide.module.css';
import config from '../../config/index';


export default function SignInSide(props) {
    
  //Defining and storing parameters in order to request API
  const { setIsAuth, setCookie } = props;
  const [isLoading, setIsLoading] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const openNotification = (type, title, message) => {
    console.log(console.log({ type, title, message }));
  };

  const handleLogin = async function () {
    if (!username) {return openNotification("warning", "Login Error", "Username parameter is missing!");}
    if (!password) {return openNotification("warning", "Login Error", "Password parameter is missing!");}

    setIsLoading(true);

    try {
      const response = await axios({
        url: config.serverUrl,
        method: "GET",
        params: {
          function: "login",
          email: username,
          password: password
        }
      });

      if(!response.data.login) {
        throw new Error("Email or password is wrong");
      }

      setIsLoading(false);

      const session = await response.data.session;
      setCookie("session", session, { maxAge: 86400 });
      setCookie("email", username, { maxAge: 86400 });
      setIsAuth(true);

      openNotification("info", "Login successed!");
    } catch(error) {
      console.log(error.message);
      setIsLoading(false);

      const { response } = error;
      if (response) {
        const { request, ...errorObject } = response;
        console.log(errorObject);
        return openNotification("warning", "Login Error", errorObject.data.message);
      }
      else {
        return openNotification("error", "Login Error", error.message);
      }
    }
  }

  return (
    isLoading ? 
    <Spin tip="Loading...">
    <Alert
      message="Alert message title"
      description="Further details about the context of this alert."
      type="info"
    />
    </Spin> :
    <div className={styles.mainLayout}>
    {/* Defining HTML Header and coonecting fontsize CSS file */}
    <Head>
        <title>Alotech Softphone</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'></link>
    </Head>
    {/* <div>
      <Image
        src="/vectorA.png"
        alt="background"
        // width={700}
        // height={700}
        layout= "fill"
        objectFit= "scale-down"
              
      />
    </div>  */}
    {/* <div className={styles.vectorB}>
    <Image
        src="/vectorB.png"
        alt="background"
        width={1000}
        height={1000}          
      />
    </div>  */}
      <div className={styles.customContainer}>
        {/* Brand logo Image */}
        <div className={styles.logoStyle}>
          <img width="350" src="/alo-tech-logo.png"/>
        </div>

        <form onSubmit={handleLogin}>
    
          <div className={styles.userInputWrp}>
            <br />
            <svg style={{ position: "absolute", top: "22px" }} width="24" height="24" viewBox="0 0 18 13" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M16.418 0.0253906H1.58203C0.710789 0.0253906 0 0.695961 0 1.51953V11.4805C0 12.3012 0.707625 12.9746 1.58203 12.9746H16.418C17.287 12.9746 18 12.3063 18 11.4805V1.51953C18 0.69875 17.2924 0.0253906 16.418 0.0253906ZM16.1995 1.02148L9.03354 7.78938L1.80559 1.02148H16.1995ZM1.05469 11.2742V1.72104L6.13403 6.47706L1.05469 11.2742ZM1.80046 11.9785L6.883 7.17834L8.66391 8.84587C8.87006 9.03891 9.20275 9.03828 9.40806 8.84434L11.1445 7.20434L16.1995 11.9785H1.80046ZM16.9453 11.2742L11.8903 6.5L16.9453 1.72579V11.2742Z" fill="#636363" />
            </svg>
            <input type="text" id="username" value={username} onChange={(e) => setUsername(e.target.value)} className={styles.inputText} required />
            <span className={styles.floatingLabel}>Email</span>
          </div>
          <br/>
          <div className={styles.userInputWrp}>
            <br/>
            <svg style={{ position: "absolute", top: "18px" }} width="24" height="25" viewBox="0 0 13 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M11.7198 7.88H10.5598V5.64C10.5598 3.45601 8.76179 1.72001 6.49978 1.72001C4.23779 1.72001 2.43979 3.45601 2.43979 5.64V7.88H1.27979V5.64C1.27979 2.84001 3.59979 0.600006 6.49978 0.600006C9.39979 0.600006 11.7198 2.84001 11.7198 5.64V7.88Z" fill="#636363" />
              <path d="M11.1401 17.4H1.86012C0.874117 17.4 0.120117 16.672 0.120117 15.72V9.00001C0.120117 8.04801 0.874117 7.32001 1.86012 7.32001H11.1401C12.1261 7.32001 12.8801 8.04801 12.8801 9.00001V15.72C12.8801 16.672 12.1261 17.4 11.1401 17.4ZM1.86012 8.44001C1.51212 8.44001 1.28012 8.66401 1.28012 9.00001V15.72C1.28012 16.056 1.51212 16.28 1.86012 16.28H11.1401C11.4881 16.28 11.7201 16.056 11.7201 15.72V9.00001C11.7201 8.66401 11.4881 8.44001 11.1401 8.44001H1.86012Z" fill="#636363" />
              <path d="M6.49984 11.8C7.14049 11.8 7.65984 11.2986 7.65984 10.68C7.65984 10.0614 7.14049 9.56 6.49984 9.56C5.85919 9.56 5.33984 10.0614 5.33984 10.68C5.33984 11.2986 5.85919 11.8 6.49984 11.8Z" fill="#636363" />
              <path d="M6.78989 10.68H6.20988L5.62988 14.04H7.36989L6.78989 10.68Z" fill="#636363" />
            </svg>
            <input type="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)} className={styles.inputText} required />
            <span className={styles.floatingLabel}>Password</span>
          </div>
          <br/>
          <div>
            <button type="submit" className={styles.btn} id="btnSubmit">Login
              <svg style={{ verticalAlign: "bottom", marginLeft: "9px" }} width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.7756 4.40077e-05C8.43335 -0.0115839 4.61217 2.28202 2.42824 5.74713C2.33143 5.9012 2.43963 6.10468 2.61901 6.10468H4.62072C4.75739 6.10468 4.88552 6.04364 4.97094 5.93608C5.17026 5.68899 5.38381 5.45062 5.60876 5.22387C6.537 4.27911 7.61615 3.53492 8.81775 3.01748C10.0592 2.4826 11.3804 2.20934 12.7443 2.20934C14.1082 2.20934 15.4293 2.47969 16.6708 3.01748C17.8724 3.53492 18.9515 4.27911 19.8798 5.22387C20.808 6.16864 21.5341 7.27038 22.0438 8.49422C22.5706 9.76165 22.8354 11.1076 22.8354 12.5C22.8354 13.8925 22.5677 15.2384 22.0438 16.5058C21.537 17.7297 20.808 18.8314 19.8798 19.7762C18.9515 20.7209 17.8724 21.4651 16.6708 21.9826C15.4284 22.5183 14.093 22.7932 12.7443 22.7907C11.3804 22.7907 10.0592 22.5174 8.81775 21.9826C7.61851 21.4657 6.52876 20.7164 5.60876 19.7762C5.38381 19.5465 5.17311 19.3081 4.97094 19.064C4.88552 18.9564 4.75454 18.8954 4.62072 18.8954H2.61901C2.43963 18.8954 2.32858 19.0988 2.42824 19.2529C4.60933 22.7093 8.41342 25 12.7443 25C19.4698 25 24.931 19.4738 24.9994 12.6279C25.0677 5.67155 19.5979 0.0174858 12.7756 4.40077e-05ZM9.16854 15.7558V13.5465H0.22779C0.102505 13.5465 0 13.4419 0 13.314V11.6861C0 11.5582 0.102505 11.4535 0.22779 11.4535H9.16854V9.24421C9.16854 9.04945 9.39064 8.93898 9.5387 9.06108L13.5791 12.3169C13.6063 12.3386 13.6284 12.3664 13.6435 12.3982C13.6587 12.4299 13.6665 12.4647 13.6665 12.5C13.6665 12.5353 13.6587 12.5702 13.6435 12.6019C13.6284 12.6336 13.6063 12.6614 13.5791 12.6832L9.5387 15.939C9.39064 16.0582 9.16854 15.9506 9.16854 15.7558Z" fill="white" />
              </svg>
            </button>
          </div>
        </form>
      </div>
         
  </div>
);
}
   











 
  

  // function handleSubmit(event) {
  //   event.preventDefault();
  //   const data = JSON.stringify({
  //     "tenant": tenant,
  //     "email": email,
  //     "password": password
  //   });
  //   const config = {
  //     method: 'post',
  //     url: '/api/login/',
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     data: data
  //   };



  //   //Requesting login API and set Cookies in according to response
  //   axios(config)
  //     .then(function (response) {
  //       console.log("..", response.data)
  //       if (response.data.code == 200 && response.data.session) {
  //         setLoginStatus(true)
  //         Cookies.set('username', email, { expires: 0.5 })
  //         Cookies.set('tenant', tenant, { expires: 0.5 })
  //         Cookies.set('message', response.data.message, { expires: 0.5 })
  //         Cookies.set('session', response.data.session, { expires: 0.5 })
  //         Cookies.set('language', response.data.language, { expires: 0.5 })
  //       } else {
  //         setLoginError(response.data.message)
  //       }
  //     })
  //     .catch(function (error) {
  //       console.log(error);
  //     });
  // }

  // //Checking session once a page
  // useEffect(() => {
  //   if (Cookies.get('session')) {
  //     setLoginStatus(true)
  //   }
  // }, [])

  // return (
  //   <div className={styles.mainLayout}>
  //     {/* Defining HTML Header and coonecting fontsize CSS file */}
  //     <Head>
  //         <title>Alotech Softphone</title>
  //         <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  //         <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'></link>
  //     </Head>
  //     <span className={styles.vectorA}>
  //       <Image src="./vectorA.png"/>
  //     </span> 
  //     <span className={styles.vectorB}>
  //       <Image src="./vectorB.png"/>
  //     </span> 
  //     {!loginStatus && (
  //       <div className={styles.customContainer}>
  //         {/* Brand logo Image */}
  //         <div className={styles.logoStyle}>
  //           <img width="350" src="/alo-tech-logo.png"/>
  //         </div>

  //         <form onSubmit={handleSubmit}>
  //           {loginError && <p style={{ color: 'red' }}>{loginError}</p>} {/* If login fails, it will be display error */}
  //           <div className={styles.userInputWrp}>
  //             <br />
  //             <svg style={{ position: "absolute", top: "22px" }} width="24" height="24" viewBox="0 0 25 15" fill="none" xmlns="http://www.w3.org/2000/svg">
  //               <path d="M20.5566 14.8242H4.44336C3.26904 14.8242 2.1585 14.3688 1.31631 13.5418C0.46748 12.7083 0 11.6031 0 10.4297C0 9.2563 0.46748 8.15103 1.31631 7.31758C1.98105 6.66484 2.81299 6.2436 3.71152 6.09502C3.71113 6.0752 3.71094 6.05522 3.71094 6.03516C3.71094 2.8043 6.33945 0.175781 9.57031 0.175781C10.7685 0.175781 11.9199 0.536475 12.9001 1.2189C13.5901 1.69927 14.163 2.31475 14.5889 3.02764C15.2764 2.60122 16.0693 2.37305 16.8945 2.37305C19.0862 2.37305 20.9083 3.98564 21.2364 6.08672C22.1549 6.22734 23.0061 6.6522 23.6837 7.31758C24.5325 8.15103 25 9.25625 25 10.4297C25 11.6031 24.5325 12.7083 23.6837 13.5418C22.8415 14.3688 21.731 14.8242 20.5566 14.8242ZM4.44336 7.5C2.80098 7.5 1.46484 8.81426 1.46484 10.4297C1.46484 12.0451 2.80098 13.3594 4.44336 13.3594H20.5566C22.199 13.3594 23.5352 12.0451 23.5352 10.4297C23.5352 8.81426 22.199 7.5 20.5566 7.5H19.8242V6.76758C19.8242 5.15215 18.51 3.83789 16.8945 3.83789C16.1247 3.83789 15.3965 4.13672 14.844 4.67935L14.0556 5.45366L13.6496 4.42588C12.9812 2.73394 11.38 1.64062 9.57031 1.64062C7.14717 1.64062 5.17578 3.61201 5.17578 6.03516C5.17578 6.2082 5.20293 6.39399 5.23164 6.59072L5.36284 7.5H4.44336Z" fill="black" />
  //             </svg>
  //             <input id="tenant" type="text" value={tenant} onChange={(e) => setTenant(e.target.value)} className={styles.inputText} required />
  //             <span className={styles.floatingLabel}>Tenant</span>
  //           </div>
  //           <br/>
  //           <div className={styles.userInputWrp}>
  //             <br />
  //             <svg style={{ position: "absolute", top: "22px" }} width="24" height="24" viewBox="0 0 18 13" fill="none" xmlns="http://www.w3.org/2000/svg">
  //               <path d="M16.418 0.0253906H1.58203C0.710789 0.0253906 0 0.695961 0 1.51953V11.4805C0 12.3012 0.707625 12.9746 1.58203 12.9746H16.418C17.287 12.9746 18 12.3063 18 11.4805V1.51953C18 0.69875 17.2924 0.0253906 16.418 0.0253906ZM16.1995 1.02148L9.03354 7.78938L1.80559 1.02148H16.1995ZM1.05469 11.2742V1.72104L6.13403 6.47706L1.05469 11.2742ZM1.80046 11.9785L6.883 7.17834L8.66391 8.84587C8.87006 9.03891 9.20275 9.03828 9.40806 8.84434L11.1445 7.20434L16.1995 11.9785H1.80046ZM16.9453 11.2742L11.8903 6.5L16.9453 1.72579V11.2742Z" fill="#636363" />
  //             </svg>
  //             <input type="text" id="username" value={email} onChange={(e) => setEmail(e.target.value)} className={styles.inputText} required />
  //             <span className={styles.floatingLabel}>Email</span>
  //           </div>
  //           <br/>
  //           <div className={styles.userInputWrp}>
  //             <br/>
  //             <svg style={{ position: "absolute", top: "18px" }} width="24" height="25" viewBox="0 0 13 18" fill="none" xmlns="http://www.w3.org/2000/svg">
  //               <path d="M11.7198 7.88H10.5598V5.64C10.5598 3.45601 8.76179 1.72001 6.49978 1.72001C4.23779 1.72001 2.43979 3.45601 2.43979 5.64V7.88H1.27979V5.64C1.27979 2.84001 3.59979 0.600006 6.49978 0.600006C9.39979 0.600006 11.7198 2.84001 11.7198 5.64V7.88Z" fill="#636363" />
  //               <path d="M11.1401 17.4H1.86012C0.874117 17.4 0.120117 16.672 0.120117 15.72V9.00001C0.120117 8.04801 0.874117 7.32001 1.86012 7.32001H11.1401C12.1261 7.32001 12.8801 8.04801 12.8801 9.00001V15.72C12.8801 16.672 12.1261 17.4 11.1401 17.4ZM1.86012 8.44001C1.51212 8.44001 1.28012 8.66401 1.28012 9.00001V15.72C1.28012 16.056 1.51212 16.28 1.86012 16.28H11.1401C11.4881 16.28 11.7201 16.056 11.7201 15.72V9.00001C11.7201 8.66401 11.4881 8.44001 11.1401 8.44001H1.86012Z" fill="#636363" />
  //               <path d="M6.49984 11.8C7.14049 11.8 7.65984 11.2986 7.65984 10.68C7.65984 10.0614 7.14049 9.56 6.49984 9.56C5.85919 9.56 5.33984 10.0614 5.33984 10.68C5.33984 11.2986 5.85919 11.8 6.49984 11.8Z" fill="#636363" />
  //               <path d="M6.78989 10.68H6.20988L5.62988 14.04H7.36989L6.78989 10.68Z" fill="#636363" />
  //             </svg>
  //             <input type="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)} className={styles.inputText} required />
  //             <span className={styles.floatingLabel}>Password</span>
  //           </div>
  //           <br/>
  //           <div>
  //             <button type="submit" className={styles.btn} id="btnSubmit">Login
  //               <svg style={{ verticalAlign: "bottom", marginLeft: "9px" }} width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
  //                 <path d="M12.7756 4.40077e-05C8.43335 -0.0115839 4.61217 2.28202 2.42824 5.74713C2.33143 5.9012 2.43963 6.10468 2.61901 6.10468H4.62072C4.75739 6.10468 4.88552 6.04364 4.97094 5.93608C5.17026 5.68899 5.38381 5.45062 5.60876 5.22387C6.537 4.27911 7.61615 3.53492 8.81775 3.01748C10.0592 2.4826 11.3804 2.20934 12.7443 2.20934C14.1082 2.20934 15.4293 2.47969 16.6708 3.01748C17.8724 3.53492 18.9515 4.27911 19.8798 5.22387C20.808 6.16864 21.5341 7.27038 22.0438 8.49422C22.5706 9.76165 22.8354 11.1076 22.8354 12.5C22.8354 13.8925 22.5677 15.2384 22.0438 16.5058C21.537 17.7297 20.808 18.8314 19.8798 19.7762C18.9515 20.7209 17.8724 21.4651 16.6708 21.9826C15.4284 22.5183 14.093 22.7932 12.7443 22.7907C11.3804 22.7907 10.0592 22.5174 8.81775 21.9826C7.61851 21.4657 6.52876 20.7164 5.60876 19.7762C5.38381 19.5465 5.17311 19.3081 4.97094 19.064C4.88552 18.9564 4.75454 18.8954 4.62072 18.8954H2.61901C2.43963 18.8954 2.32858 19.0988 2.42824 19.2529C4.60933 22.7093 8.41342 25 12.7443 25C19.4698 25 24.931 19.4738 24.9994 12.6279C25.0677 5.67155 19.5979 0.0174858 12.7756 4.40077e-05ZM9.16854 15.7558V13.5465H0.22779C0.102505 13.5465 0 13.4419 0 13.314V11.6861C0 11.5582 0.102505 11.4535 0.22779 11.4535H9.16854V9.24421C9.16854 9.04945 9.39064 8.93898 9.5387 9.06108L13.5791 12.3169C13.6063 12.3386 13.6284 12.3664 13.6435 12.3982C13.6587 12.4299 13.6665 12.4647 13.6665 12.5C13.6665 12.5353 13.6587 12.5702 13.6435 12.6019C13.6284 12.6336 13.6063 12.6614 13.5791 12.6832L9.5387 15.939C9.39064 16.0582 9.16854 15.9506 9.16854 15.7558Z" fill="white" />
  //               </svg>
  //             </button>
  //           </div>
  //         </form>
  //       </div>
  //     )}{loginStatus && (
  //       <div className={styles.container}>
  //         <DashBoard/>
  //       </div>
  //     )}       
  //   </div>
  // );

