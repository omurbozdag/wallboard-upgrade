import React, { useEffect, useState } from "react";
import { Table, Tag } from 'antd';
import axios from "axios";
import "antd/dist/antd.css";
import styles from '../../../styles/AgentTableComponent.module.css';
import "@fontsource/reem-kufi";

import config from '../../config/index';

export default function AgentTableComponent(props) {
  const [state, setState] = useState([]);
  const [loading, setLoading] = useState(true);
    
  useEffect(() => {
    const interval = setInterval(() => {
      getData()
    }, 5000);
    return () => clearInterval(interval);
  }, [])

  const getData = async () => {
    const data = JSON.stringify({
      "tenant": config.serverUrl,
      "session": props.session,
    });
    const settingTable = {
      method: 'post',
      url: '/api/monitoringAgentStatistic2/',
      headers: {
        'Content-Type': 'application/json'
      },
      data: data
    };
    //Requesting statsWaiting API using by axios
    axios(settingTable)
      .then(function (response) {
        if (response.data.code == 200 && response.data.success) {
          setState(response.data.dataAgentTable)
          setLoading(false);
        } else {
          var demo = "-" // düzeltilecek
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  var All = 260;
  
  const sorter = (a, b) => (isNaN(a) && isNaN(b) ? (a || '').localeCompare(b || '') : a - b);

  const columns = [
    {
      title: 'Agent',
      className: 'agentTag',
      dataIndex: 'name',
      key: 'name',
      width: '9vw',
      sortDirections: ['descend', 'ascend'],
      sorter: (a, b) => sorter(a.name, b.name),
    },
    {
      title: 'Status',
      dataIndex: 'statu',
      key: 'statu',
      align: 'center',
      sorter: (a, b) => sorter(a.statu, b.statu),
      render: tag => {
        let color = '';
        if (tag === "available") {
          color = 'green';
        }
        else if (tag === "aftercallwork") {
          color = 'volcano';
        }
        else if (tag === "dialing") {
          color = 'red';
        }
        else if (tag === 'shortbreak') {
          color = 'orange';
        }
        else if (tag === 'lunch') {
          color = 'blue';
        }
        else if (tag === 'wrapup') {
          color = 'magenta';
        }
        else if (tag === 'education') {
          color = 'purple';
        }
        else if (tag === 'talking') {
          color = 'gold';
        }
        else{
          color = 'cyan';
        }; return (
          <>
            <Tag color={color} key={tag} className={styles.statusTag}>
              {tag}
            </Tag>
          </>
        )
      },
    },
    {
      title: 'Duration',
      dataIndex: 'status_update',
      key: 'status_update',
      align: 'center',
    },
    {
      title: 'Inbound',
      dataIndex: 'inbound_calls',
      key: 'inbound_calls',
      align: 'center',
    },
    {
      title: 'Outbound',
      dataIndex: 'outbound_calls',
      key: 'outbound_calls',
      align: 'center',
    },
    {
      title: 'Total Break',
      dataIndex: 'total_break',
      key: 'total_break',
      align: 'center',
    },
    {
      title: 'Total Extra',
      dataIndex: 'total_extra',
      key: 'total_extra',
      align: 'center',
    },
  ];

  return (
    <div className={styles.container} className="data_table">
      {loading ? (
        "Loading"
      ) : (
          <Table className={styles.table} pagination={{ defaultPageSize: 20, pageSizeOptions: [10, 20, 50, 100, All]}} columns={columns} dataSource={state} scroll={{ x: '40vw', y: '83vh' }} />
        )}
    </div>
  );
}


// const columns = [
//   {
//     title: 'Agent',
//     style: {
//       title: {fontSize: 50,}
//     },
//     dataIndex: 'name',
//     key: 'name',
//     sortDirections: ['descend', 'ascend'],
//     sorter: (a, b) => sorter(a.name, b.name),
//     render: tag => {
//        return (
//         <>
//           <h3 className={styles.agentTag} key={tag}>
//             {tag}
//           </h3>
//         </>
//       )
//     },
//   },
//   {

//     title: 'Status',
//     dataIndex: 'statu',
//     key: 'statu',
//     sorter: (a, b) => sorter(a.statu, b.statu),
//     render: tag => {
//       let color = '';
//       if (tag === "available") {
//         color = 'green';
//       }
//       else if (tag === "aftercallwork") {
//         color = 'volcano';
//       }
//       else if (tag === "dialing") {
//         color = 'red';
//       }
//       else if (tag === 'shortbreak') {
//         color = 'orange';
//       }
//       else if (tag === 'lunch') {
//         color = 'blue';
//       }
//       else if (tag === 'wrapup') {
//         color = 'magenta';
//       }
//       else if (tag === 'education') {
//         color = 'purple';
//       }
//       else if (tag === 'talking') {
//         color = 'gold';
//       }
//       else{
//         color = 'cyan';
//       }; return (
//         <>
//           <Tag color={color} key={tag} className={styles.statusTag}>
//             {tag}
//           </Tag>
//         </>
//       )
//     },
//   },
//   {
//     title: 'Duration',
//     dataIndex: 'status_update',
//     key: 'status_update',
//     render: tag => {
//        return (
//         <>
//           <h3  className={styles.durationTag} key={tag}>
//             {tag}
//           </h3>
//         </>
//       )
//     },
//   },
//   {
//     title: 'Inbound',
//     dataIndex: 'inbound_calls',
//     key: 'inbound_calls',
//     render: tag => {
//       return (
//        <>
//          <h3 className={styles.inboundTag} key={tag}>
//            {tag}
//          </h3>
//        </>
//      )
//    },
//   },
//   {
//     title: 'Outbound',
//     dataIndex: 'outbound_calls',
//     key: 'outbound_calls',
//     render: tag => {
//       return (
//        <>
//          <h3 className={styles.outboundTag} key={tag}>
//            {tag}
//          </h3>
//        </>
//      )
//    },
//   },
//   {
//     title: 'Total Break',
//     dataIndex: 'total_break',
//     key: 'total_break',
//     render: tag => {
//       return (
//        <>
//          <h3 className={styles.totalBreakTag} key={tag}>
//            {tag}
//          </h3>
//        </>
//      )
//    },
//   },
//   {
//     title: 'Total Extra',
//     dataIndex: 'total_extra',
//     key: 'total_extra',
//     render: tag => {
//       return (
//        <>
//          <h3 className={styles.totalExtraTag} key={tag}>
//            {tag}
//          </h3>
//        </>
//      )
//    },
//   },
// ];
