import { Card } from 'antd';

import 'antd/dist/antd.css';
import "@fontsource/reem-kufi";

export default function CardComponent(props) {

    return (
        <div >
            <Card 
                title={props.name} 
                bordered={false}
                size="small"
                headStyle={{
                    backgroundColor: props.color, 
                    textAlign: 'center', 
                    color: 'white',
                    fontFamily: 'Reem Kufi',
                    fontSize: '1.5vw',     
                }}
                bodyStyle={{ 
                    backgroundColor: '#FFF',
                    textAlign: 'center', 
                    color: '#4F4F4F',
                    fontFamily: 'Reem Kufi',
                    fontSize: '3.5vw',
                    borderRadius: 20,  
                }}
                style={{
                    borderRadius: 20,
                }}      
            >
                {props.value || props.value == 0 ? props.value : "-"}
            </Card>
        </div>
    );
};


