import React, { useEffect, useState } from "react";
import { useCookies } from 'react-cookie';
import axios from "axios";

import config from '../../config/index';


export default function Home() {
    const [state, setState] = useState([]);
    const [loading, setLoading] = useState(true);
    const [queueData, setQueueData] = useState({}); // For storing queue data that is received from API
    const [statWaitingData, setStatWaitingData] = useState({}); // For storing waiting calls data that is received from API
    const [status, setStatus] = useState("");
    const [userName, setUserName] = useState("");


    let session = cookies.session; // To get session value from cookies

    const [cookies, setCookie, removeCookie] = useCookies(['session']);

    const data = JSON.stringify({
        "endPoint": config.serverUrl,
        "session": props.session,
    });

    //trigger of getting data from all APIs by 5 seconds
    useEffect(() => {
        const interval = setInterval(() => {
            getData_MonitoringAgentStatistic2()
            getData_MonitoringQueue()
            getData_StatsWaitingCalls()
            getData_Mysipinfo()
        }, 5000);
        return () => clearInterval(interval);
    }, [])

    //function of getting data
    const getData_MonitoringAgentStatistic2 = async () => {
        const setting = {
          method: 'post',
          url: '/api/monitoringAgentStatistic2/',
          headers: {
            'Content-Type': 'application/json'
          },
          data: data
        };
        //Requesting statsWaiting API using by axios
        axios(setting)
            .then(function (response) {
                if (response.data.code == 200 && response.data.success) {
                    setState(response.data.dataAgentTable)
                    setLoading(false);
                } else {
                    var demo = "-" // düzeltilecek
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    const getData_MonitoringQueue = async () => {
        const setting = {
            method: 'post',
            url: '/api/monitoringQueue/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        // Requesting monitoringQueue API using by axios
        axios(setting)
            .then(function (response) {
                if (response.data.code == 200 && response.data.success) {
                    setQueueData(response.data)
                } else {
                    var demo = "-" // düzeltilecek
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    const getData_StatsWaitingCalls = async () => {
        const setting = {
            method: 'post',
            url: '/api/statsWaitingCalls/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
          //Requesting statsWaiting API using by axios
        axios(setting)
            .then(function (response) {
                if (response.data.code == 200 && response.data.success) {
                    setStatWaitingData(response.data)
                } else {
                    var demo = "-" // düzeltilecek
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    const getData_Mysipinfo = async () => {
        const setting = {
            method: 'post',
            url: '/api/getmysipinfo/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        // Requesting monitoringQueue API using by axios
        axios(setting)
            .then(function (response) {
                if (response.data.code == 200 && response.data.success) {
                    setUserName(response.data)
                } else {
                    setUserName("")
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        }




 
  

  
    








    return (
        <div>
            home
        </div>
    )
}






const getData = async () => {
    const data = JSON.stringify({
      "tenant": config.serverUrl,
      "session": props.session,
    });
    const settingTable = {
      method: 'post',
      url: '/api/monitoringAgentStatistic2/',
      headers: {
        'Content-Type': 'application/json'
      },
      data: data
    };
    //Requesting statsWaiting API using by axios
    axios(settingTable)
      .then(function (response) {
        if (response.data.code == 200 && response.data.success) {
          setState(response.data.dataAgentTable)
          setLoading(false);
        } else {
          var demo = "-" // düzeltilecek
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }