import React, {useEffect, useState} from 'react';
import { useCookies } from 'react-cookie';
import axios from 'axios';
import { Row, Col, Space, Divider, Card } from 'antd';
import styles from '../../../styles/Dashboard.module.css';
import 'antd/dist/antd.css';
import config from '../../config/index';

import PageHeaderComponent from './PageHeaderComponent';
import CardComponent from './CardComponent';
import StatusComponent from './StatusComponent';
import ChartComponent from './ChartComponent';
import DurationWaitingComponent from './DurationWaitingComponent';
import AgentTableComponent from './AgentTableComponent';


export default function Dashboard() {
    const [cookies, setCookie, removeCookie] = useCookies(['session']);
    const [queueData, setQueueData] = useState({}); // For storing queue data that is received from API
    const [statWaitingData, setStatWaitingData] = useState({}); // For storing waiting calls data that is received from API
    const [inboundCall, setInboundCall] = useState();
    const [agentStatistic2Data, setAgentStatistic2Data] = useState({});
    let session = cookies.session; // To get session value from cookies
    
    useEffect(() => {
        const data = JSON.stringify({
            "tenant": config.serverUrl,
            "session": session,
        });
    
        const settingQueue = {
            method: 'post',
            url: '/api/monitoringQueue/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        // Requesting monitoringQueue API using by axios
        axios(settingQueue)
            .then(function (response) {
                if (response.data.code == 200 && response.data.success) {
                    setQueueData(response.data)
                } else {
                    var demo = "-" // düzeltilecek
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    
        const settingWaiting = {
            method: 'post',
            url: '/api/statsWaitingCalls/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        //Requesting statsWaiting API using by axios
        axios(settingWaiting)
            .then(function (response) {
                if (response.data.code == 200 && response.data.success) {
                    setStatWaitingData(response.data)
                } else {
                    var demo = "-" // düzeltilecek
                }
            })
            .catch(function (error) {
                console.log(error);
            });

        const settingAgentStatistic2 = {
            method: 'post',
            url: '/api/monitoringAgentStatistic2/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        // Requesting monitoringQueue API using by axios
        axios(settingAgentStatistic2)
            .then(function (response) {
                if (response.data.code == 200 && response.data.success) {
                    setAgentStatistic2Data(response.data)
                } else {
                    var demo = "-" // düzeltilecek
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        


      }, []);

    var ar2 = Math.floor((queueData.total_queue_answered / agentStatistic2Data.total_inbould_call) * 100);

    // console.log("demo", inboundCall)  
    // value={inboundCall[0].inbound_calls}
    return (
        <div className={styles.container}>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col className="gutter-row" span={24}> 
                    <PageHeaderComponent session={session}/>
                </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} >
                {/* <Col className="gutter-row" span={13}> */}
                <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={13}>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col className="gutter-row" xs={6} sm={6} md={6} lg={6} xl={6} xxl={6}>
                            <CardComponent name="Inbound Call" color="#66BB6A" value={agentStatistic2Data.total_inbould_call} />
                        </Col>
                        <Col className="gutter-row" xs={6} sm={6} md={6} lg={6} xl={6} xxl={6}>
                            <CardComponent name="Outbound Call" color="#4CAF50" value={queueData.total_queue_outbound}/>
                        </Col>
                        <Col className="gutter-row" xs={6} sm={6} md={6} lg={6} xl={6} xxl={6}>
                            <CardComponent name="Answered Call" color="#43A047" value={queueData.total_queue_answered}/>
                        </Col>
                        <Col className="gutter-row" xs={6} sm={6} md={6} lg={6} xl={6} xxl={6}>
                            <CardComponent name="Short Call" color="#388E3C" value={queueData.total_queue_shortcalls}/>
                        </Col>
                    </Row>
                    <br/>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col className="gutter-row" xs={6} sm={6} md={6} lg={6} xl={6} xxl={6}>
                            <CardComponent name="Active Agent" color="#F9A825" value={agentStatistic2Data.active_count} />
                        </Col>
                        <Col className="gutter-row" xs={6} sm={6} md={6} lg={6} xl={6} xxl={6}>
                            <CardComponent name="Active Call" color="#FF7543"/>
                        </Col>
                        <Col className="gutter-row" xs={6} sm={6} md={6} lg={6} xl={6} xxl={6}>
                            <CardComponent name="Waiting Call" color="#EF6C00" value={statWaitingData.waiting_calls}/>
                        </Col>
                        <Col className="gutter-row" xs={6} sm={6} md={6} lg={6} xl={6} xxl={6}>
                            <CardComponent name="Abandon Call" color="#D84315" value={queueData.total_queue_abandon}/>
                        </Col>
                    </Row>
                    <br/>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col className="gutter-row" xs={14} sm={14} md={14} lg={14} xl={14} xxl={14}>
                            <StatusComponent session={session} a={setInboundCall}/>
                        </Col>
                        <Col className="gutter-row" xs={10} sm={10} md={10} lg={10} xl={10} xxl={10}>
                            <ChartComponent sl={queueData.sl} sl2={queueData.sl2} ssl={queueData.ssl} ssl2={queueData.ssl2} ar2={ar2} />
                        </Col>
                    </Row>
                    <br/>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col className="gutter-row" xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                            <DurationWaitingComponent />
                        </Col>
                    </Row>
                </Col>
                {/* <Col className="gutter-row" span={11}> */}
                <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={11}>
                    <AgentTableComponent session={session}/>
                </Col>
            </Row>
        </div>
    );
};
