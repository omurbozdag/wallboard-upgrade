import React, {useState, useEffect} from 'react';
import { useCookies } from 'react-cookie';
import Image from 'next/image';
import axios from 'axios';
import { Button } from 'antd';
import { createFromIconfontCN, UserOutlined } from '@ant-design/icons';
import styles from '../../../styles/PageHeaderComponent.module.css';
import "@fontsource/reem-kufi";
import config from '../../config/index';
import SignInSide from '../login/SignInSide'; //düzenlenecek
import Dashboard from '../home/Dashboard'; //düzenlenecek

const style = { background: '#0092ff', padding: '8px 0' };
const IconFont = createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js',
});

export default function PageHeaderComponent(props) {

    const [cookies, setCookie, removeCookie] = useCookies(['session']);
    
    console.log(cookies)

    const [userName, setUserName] = useState("");
    const [logging, setLogging] =useState(true);
    const data = JSON.stringify({
        "tenant": config.serverUrl,
        "session": props.session,
    });

    useEffect(() => {
        const setting = {
            method: 'post',
            url: '/api/getmysipinfo/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        // Requesting monitoringQueue API using by axios
        axios(setting)
            .then(function (response) {
                if (response.data.code == 200 && response.data.success) {
                    setUserName(response.data)
                } else {
                    setUserName("")
                }
            })
            .catch(function (error) {
                console.log(error);
            });
      }, []);
   
    function handleLogout() {
        const settingLogout = {
            method: 'post',
            url: '/api/logout/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        // Requesting monitoringQueue API using by axios
        axios(settingLogout)
            .then(function (response) {
                if (response.data.code == 200) {
                    setLogging(!response.data)
                    removeCookie('session')
                    return <SignInSide/>
                } else {
                    setLogging(false) //
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }  

    return (
        <div className={styles.container}>
            <div className={styles.logo}>
                <Image
                    src="/nestle-logo.png"
                    alt="logo of brand"
                    width={170}
                    height={50}
                    className={styles.brand}
                />
            </div>
            <div className={styles.wrapper}>
                <p className={styles.name}>{userName.name}</p>
                <UserOutlined className={styles.icon} />
                <a type="submit" onClick={handleLogout}>
                <IconFont className={styles.icon} type="icon-tuichu" />
                </a>
            </div>
        </div>        
    );
};


