import React, {useEffect, useState} from 'react';
import axios from 'axios';
import config from '../../config/index';
import styles from '../../../styles/StatusComponent.module.css';
import { Progress, Row, Col } from 'antd';

export default function StatusComponent(props) {
    const [status, setStatus] = useState("");

    useEffect(() => {
      const data = JSON.stringify({
        "tenant": config.serverUrl,
        "session": props.session,
      });
      
      const settingQueue = {
        method: 'post',
        url: '/api/monitoringAgentStatistic2/',
        headers: {
          'Content-Type': 'application/json'
        },
        data: data
      };
      // Requesting monitoringAgentStatistic2 API using by axios
      axios(settingQueue)
        .then(function (response) {
          if (response.data.code == 200 && response.data.success) {
            setStatus(response.data)
          } else {
            var demo = "-" // düzeltilecek
          }
        })
        .catch(function (error) {
          console.log(error);
        }); 
    }, []);
    
  
  console.log("demo", status)
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Status</h1>
      <Row>
        <Col className="gutter-row" span={10} offset={1}>
          <div className={styles.wrapper}>
              <h3 className={styles.font}>Available</h3>
              <h4 className={styles.font}>{status.available_count}</h4> 
              {/* <h4 className={styles.font}>94</h4>  */}
          </div>           
          <Progress percent={status.available_count} strokeColor="#7F7FD5" trailColor="#EFF3F9" strokeWidth={15} status="active" showInfo={false} />
          {/* <Progress percent="94" strokeColor="#7F7FD5" trailColor="#EFF3F9" strokeWidth={15} status="active" showInfo={false} /> */}
          <div className={styles.wrapper}>
              <h3 className={styles.font}>Brake</h3>
              <h4 className={styles.font}>{status.shortbreak_count}</h4>
          </div>
          <Progress percent={status.shortbreak_count} strokeColor="#7F7FD5" trailColor="#EFF3F9" strokeWidth={15} status="active" showInfo={false} />
          <div className={styles.wrapper}>
              <h3 className={styles.font}>Lunch</h3>
              <h4 className={styles.font}>{status.lunch_count}</h4>
          </div>
          <Progress percent={status.lunch_count} strokeColor="#7F7FD5" trailColor="#EFF3F9" strokeWidth={15} status="active" showInfo={false} />
        </Col>
        <Col className="gutter-row" span={10} offset={2} style={{paddingBottom: '3vh'}}>
          <div className={styles.wrapper}>
              <h3 className={styles.font}>Back Office</h3>
              <h4 className={styles.font}>{status.backoffice_count}</h4>
          </div>
          <Progress percent={status.backoffice_count} strokeColor="#7F7FD5" trailColor="#EFF3F9" strokeWidth={15} status="active" showInfo={false} />
          <div className={styles.wrapper}>
              <h3 className={styles.font}>ACW</h3>
              <h4 className={styles.font}>{status.aftercallwork_count}</h4>
          </div> 
          <Progress percent={status.aftercallwork_count} strokeColor="#7F7FD5" trailColor="#EFF3F9" strokeWidth={15} status="active" showInfo={false} />
          <div className={styles.wrapper}>
              <h3 className={styles.font}>Other</h3>
              <h4 className={styles.font}>{status.other_count}</h4>
          </div>
          <Progress percent={status.other_count} strokeColor="#7F7FD5" trailColor="#EFF3F9" strokeWidth={15} status="active" showInfo={false} />
        </Col>
      </Row>
    </div>
  );
};

