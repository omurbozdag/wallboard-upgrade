import React from 'react';

import { Carousel, Progress } from 'antd';
import styles from '../../../styles/ChartComponent.module.css';


export default function ChartComponent(props) {

  return (
    <div className={styles.container}>
      <Carousel  
        className={styles.carousel}
        adaptiveHeight={true}
        autoplay
      >
        <div className={styles.wrapper}>
          <h1 className={styles.title}>SL</h1>
          <div className={styles.percent}>%{props.ssl}</div>
          <Progress type="circle" percent={props.ssl} status="active" 
              width={160}
              className={styles.progress}
              showInfo={false}
              strokeColor="#7F7FD5"
          />
        </div>
        <div className={styles.wrapper}>
          <h1 className={styles.title}>SL2</h1>
          <div className={styles.percent}>%{props.ssl2}</div>
          <Progress type="circle" percent={props.ssl2} 
              width={160}
              className={styles.progress}
              showInfo={false}
              strokeColor="#7F7FD5"
          />
        </div>
        <div className={styles.wrapper}>
          <h1 className={styles.title}>AR</h1>
          <div className={styles.percent}>%{props.ar2}</div>
          <Progress type="circle" percent={props.ar2} 
              width={160}
              className={styles.progress}
              showInfo={false}
              strokeColor="#7F7FD5"
          />
        </div> 
      </Carousel>
    </div>
  );
};