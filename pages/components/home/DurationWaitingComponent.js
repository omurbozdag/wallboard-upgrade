import React from 'react';
import { Carousel } from "antd";
import styles from '../../../styles/DurationWaitingComponent.module.css';

export default function DurationWaitingComponent() {
    return (
        <div className={styles.container}>
            <Carousel autoplay className={styles.carousel}>
                <div className={styles.content}>
                    <div className={styles.wrapper}>
                        <div >
                            <h3 className={styles.h3Style}>Total Duration</h3>
                            <hr className={styles.hrStyle}></hr>
                            <h1 className={styles.h1Style}>__:__:__</h1>
                        </div>
                        <div>
                            <h3 className={styles.h3Style}>Awerage Duration</h3>
                            <hr className={styles.hrStyle}></hr>
                            <h1 className={styles.h1Style}>__:__:__</h1>
                        </div>
                    </div>
                </div>
                <div className={styles.content}>
                    <div className={styles.wrapper}>
                        <div>
                            <h3 className={styles.h3Style}>Total Waiting</h3>
                            <hr className={styles.hrStyle}></hr>
                            <h1 className={styles.h1Style}>__:__:__</h1>
                        </div>
                        <div>
                            <h3 className={styles.h3Style}>Awerage Waiting</h3>
                            <hr className={styles.hrStyle}></hr>
                            <h1 className={styles.h1Style}>__:__:__</h1>
                        </div>
                    </div>
                </div>
            </Carousel>  
        </div>
    );
};
