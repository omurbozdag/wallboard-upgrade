const axios = require('axios');
const qs = require('qs');

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}
export default async  (req, res) => {
    try {
        const { tenant, email, password } = req.body;
        const data = {
            email: email,
            password: password
        }
        const response = await axios.post(`https://${tenant}/api/?function=login`, qs.stringify(data), config);
        console.log("response", response)
        if (response.status == 404){
            return res.status(200).json({ code: 401, message: response.data.message})
        }else if (response.status != 200){
            return res.status(200).json({ code: 401, message: response.data.message})
        }else if (response.content != 'login')
        return res.status(200).json({code: 200, message: response.data.message, session: response.data.session, language: response.data.language, login: response.data.login })

    } catch (error) {
        return res.status(500).json({ message: 'An error occurred during login' })
    }
}