const axios = require('axios');
const qs = require('qs');

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}
export default async  (req, res) => {
    try {
        const { tenant, session } = req.body;
        const data = {
            session: session
        }
        //To get data from monitoringQueue API
        const response = await axios.post(`${tenant}?function=monitoringQueue`, qs.stringify(data), config);
      
        if (response.status == 404){
            return res.status(200).json({ code: 401, message: "UNAUTHORIZED"})
        }else if (response.status != 200){
            return res.status(200).json({ code: 401, message: "UNAUTHORIZED"})
        }
        else {
            var queueData = response.data.queuestatistics.map( row => {
                return {
                    numberofcalls: row.numberofcalls,
                    shortcalls: row.shortcalls,
                    abandon: row.abandon,
                    answered: row.answered,
                    answeredinsl: row.answeredinsl,
                    waitduration: row.waitduration,
                    talkduration: row.talkduration,
                    outbound : row.numberofoutbound
                } 
            }) 
            
            var total_queue_numberofcalls = 0
            var total_queue_shortcalls = 0
            var total_queue_abandon = 0
            var total_queue_answered = 0
            var total_queue_answeredinsl = 0
            var total_queue_waitduration = 0
            var total_queue_talkduration = 0
            var total_queue_outbound = 0
            
            for (let i = 0; i < queueData.length; i++) {
                total_queue_numberofcalls += queueData[i].numberofcalls;
                total_queue_shortcalls += queueData[i].shortcalls;
                total_queue_abandon += queueData[i].abandon;
                total_queue_answered += queueData[i].answered;
                total_queue_answeredinsl += queueData[i].answeredinsl;
                total_queue_waitduration += Number(queueData[i].waitduration);
                total_queue_talkduration += Number(queueData[i].talkduration);         
                total_queue_outbound += queueData[i].outbound;         
            }
            var sl = Math.floor(
                (total_queue_answeredinsl / total_queue_answered) ? (total_queue_answeredinsl / total_queue_answered) : 1
                 * 100
            );

            var ssl = Math.floor((total_queue_answeredinsl / total_queue_answered) * 100);

            var sl2 = Math.floor(
                (total_queue_answeredinsl / (total_queue_numberofcalls - total_queue_shortcalls)) ?  (total_queue_answeredinsl / (total_queue_numberofcalls - total_queue_shortcalls)) : 1
                 * 100
                );
            
            var ssl2 = Math.floor((total_queue_answeredinsl / (total_queue_numberofcalls - total_queue_shortcalls)) * 100);

            var ar = total_queue_answered;
        }        
        return res.status(200).json({
            code: 200,
            success: response.data.success,
            total_queue_numberofcalls: total_queue_numberofcalls,
            total_queue_shortcalls: total_queue_shortcalls,
            total_queue_abandon: total_queue_abandon,
            total_queue_answered: total_queue_answered,
            // total_queue_answeredins: total_queue_answeredinsl,
            total_queue_waitduration: total_queue_waitduration, 
            total_queue_talkduration: total_queue_talkduration, 
            total_queue_outbound: total_queue_outbound,
            sl: sl, 
            sl2: sl2,
            ssl: ssl,
            ssl2: ssl2,   
        })

    } catch (error) {
        return res.status(500).json({ message: 'An error occurred during login' })
    }
}