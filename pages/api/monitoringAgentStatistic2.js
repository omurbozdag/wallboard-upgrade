const axios = require('axios');
const qs = require('qs');

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}

function convert(time) {
    var c_minute = Math.floor(time / 60);
    var c_second = Math.floor(time % 60);
    var c_hour = Math.floor(c_minute / 60);
    c_minute = Math.floor(c_minute % 60);
    if (c_second < 10) {
      c_second = '0' + c_second;
    }
    if (c_minute < 10) {
      c_minute = '0' + c_minute;
    }
    if (c_hour < 10) {
      c_hour = '0' + c_hour;
    }
    var ctime = c_minute + ':' + c_second;
    return ctime;
}

export default async  (req, res) => {
    try {
        const { tenant, session } = req.body;
        const data = {
            session: session
        }
        //To get data from monitoringAgentStatistic2 API
        const response = await axios.post(`${tenant}?function=monitoringAgentStatistic2`, qs.stringify(data), config);
   
        if (response.status == 404){
            return res.status(200).json({ code: 401, message: "UNAUTHORIZED"})
        }else if (response.status != 200){
            return res.status(200).json({ code: 401, message: "UNAUTHORIZED"})
        }
        else {
            var statusData = response.data ? response.data.agentstatus.map(row => {
                return {
                    status: row.status
                }   
            }) : []
            var agentStatisticsData = response.data ? response.data.agentstatistics.map(row => {
                return {
                    inboundcalls: row.inboundcalls
                }   
            }) : []

            var total_inbould_call = 0

            for (let i = 0; i < agentStatisticsData.length; i++) {
                total_inbould_call += agentStatisticsData[i].inboundcalls   
            }

            var available_count = statusData.filter(item => item.status === "available").length;
            var active_count = statusData.filter(item => item.status === "talking").length;
            var backoffice_count = statusData.filter(item => item.status === "backoffice").length;
            var aftercallwork_count = statusData.filter(item => item.status === "aftercallwork").length;
            var shortbreak_count = statusData.filter(item => item.status === "shortbreak").length;
            var lunch_count = statusData.filter(item => item.status === "lunch").length;
            var other_count = statusData.length - (available_count + backoffice_count + aftercallwork_count + shortbreak_count + lunch_count)

            //agentTable Data
            var dataAgentTable = [];

            for (var i = 0; i < response.data.agentstatus.length; i++) {
                for (var j = 0; j < response.data.agentstatistics.length; j++) {
                    if (response.data.agentstatus[i].agent === response.data.agentstatistics[j].agent) {
                        if (response.data.agentstatus[i].status !== "logoff") {
                            var inbound_calls = response.data.agentstatistics[j].inboundcalls;
                            var outbound_calls = response.data.agentstatistics[j].outboundcalls;
                            var efficiency = response.data.agentstatistics[j].efficiency;
                            var total_break = convert(response.data.agentstatistics[j].totalbreak);
                            var total_extra = convert(response.data.agentstatistics[j].totalextra);
        
                            dataAgentTable.push({
                                'name': (response.data.agentstatus[i].agentname).toLowerCase(),
                                'statu': response.data.agentstatus[i].status.toLowerCase(),
                                'direction': response.data.agentstatus[i].call_direction,
                                'status_update': convert(response.data.agentstatus[i].statusupdate),
                                'inbound_calls': inbound_calls,
                                'outbound_calls': outbound_calls,
                                'efficiency': efficiency,
                                'total_break': total_break,
                                'total_extra': total_extra,
                            })
                        }
                        if (response.data.agentstatus[i].status == "logoff") {
                            var i = 0;
                            i++
                        }
                    }
                }
            }
        }
        return res.status(200).json({
            code: 200,
            success: response.data.success,
            available_count: available_count,
            backoffice_count: backoffice_count,
            aftercallwork_count: aftercallwork_count,
            shortbreak_count: shortbreak_count,
            lunch_count: lunch_count,
            other_count: other_count,
            total_inbould_call: total_inbould_call,
            dataAgentTable: dataAgentTable,
            active_count: active_count
            
        })
    } catch (error) {
        return res.status(500).json({ message: 'An error occurred during login' })
    }
}