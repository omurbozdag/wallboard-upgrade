const axios = require('axios');
const qs = require('qs');

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}
export default async  (req, res) => {
    try {
        const { tenant, session } = req.body;
        const data = {
            session: session
        }
        //To get data from statsWaitingCalls API
        const response = await axios.post(`${tenant}?function=statsWaitingCalls`, qs.stringify(data), config);
        
        if (response.status == 404){
            return res.status(200).json({ code: 401, message: "UNAUTHORIZED"})
        }else if (response.status != 200){
            return res.status(200).json({ code: 401, message: "UNAUTHORIZED"})
        }
        else if (response.data.success) {
            return res.status(200).json({
                code: 200,
                success: response.data.success,
                waiting_calls: response.data.stats.waiting_calls.length > 0 ? response.data.stats.waiting_calls.waiting_count : 0
            })
        }
    } catch (error) {
        return res.status(500).json({ message: 'An error occurred during login' })
    }
}