const axios = require('axios');
const qs = require('qs');

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}
export default async  (req, res) => {
    try {
        const { tenant, session } = req.body;
        const data = {
            session: session
        }

        const response = await axios.post(`${tenant}?function=logout`, qs.stringify(data), config);
      
        if (response.status == 404){
            return res.status(200).json({ code: 401, message: "UNAUTHORIZED"})
        }else if (response.status != 200){
            return res.status(200).json({ code: 401, message: "UNAUTHORIZED"})
        }else {
            return res.status(200).json({code: 200, logout: response.data.logout})
        }
    } catch (error) {
        return res.status(500).json({ message: 'An error occurred during login' })
    }
}