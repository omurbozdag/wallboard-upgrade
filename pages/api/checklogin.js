const axios = require('axios');
const qs = require('qs');

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}
export default async  (req, res) => {
    try {
        const { tenant, session } = req.body;
        const data = {
            session: session
        }
        const response = await axios.post(`https://${tenant}/api/?function=checklogin`, qs.stringify(data), config);

        if (response.status == 404){
            return res.status(200).json({ code: 401, message: response.data.message})
        }else if (response.status != 200){
            return res.status(200).json({ code: 401, message: response.data.message})
        }else if (response.content != 'login')
        return res.status(200).json({code: 200, login: response.data.login})

    } catch (error) {
        return res.status(500).json({ message: 'An error occurred during login' })
    }
}